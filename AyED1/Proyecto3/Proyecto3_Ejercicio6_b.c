//Esta linea permite reutilizar el codigo del ejercicio anterior sin reescribirlo. Se llama "importar libreria"
#include "entradas.c"

int main (void) {
	int x, y, z, m;

	x = pedirEntero ('x');
	y = pedirEntero ('y');
	z = pedirEntero ('z');
	m = pedirEntero ('m');

	if (x < y) {
		m = x;
	} else {
		m = y;
	}

	if (m >= z) {
		m = z;
	}

	imprimeEntero (x, 'x');
	imprimeEntero (y, 'y');
	imprimeEntero (z, 'z');
	imprimeEntero (m, 'm');

	return 0;
}

#include <stdio.h>

//Tienen una ligera modificacion para adaptarse al ejercicio siguiente
int pedirEntero (char c) {
	int x;

	printf ("Ingrese un valor para %c: ", c);
	scanf ("%d", &x);

	return x;
}

void imprimeEntero (int x, char c) {
	printf ("%c: %d\n", c, x);
}

//Se llama my_main para evitar colision de nombre
//Si se quiere probar este ejercicio debe cambiarse el nombre
//a main, pero no se podra probar el siguiente ejercicio hasta
//que vuelva a la normalidad
int my_main (void) {
	imprimeEntero (pedirEntero ('x'), 'x');

	return 0;
}

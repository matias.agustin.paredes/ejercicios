#include <stdbool.h>
#include <stdio.h>

//Funcion que imprime True o False segun corresponda
void imprime_bool (bool b) {
	if (b) {
		printf ("True\n");
	} else {
		printf ("False\n");
	}
}

int main (void) {
	int x = 4, y = -4, z = 8;
	bool b = true, w = true;

	printf ("x %% 4 == 0: ");
	imprime_bool (x % 4 == 0);

	printf ("x + y == 0 && y - x == (-1) * z: ");
	imprime_bool (x + y == 0 && y - x == (-1) * z);

	printf ("!b && w: ");
	imprime_bool (!b && w);

	return 0;
}

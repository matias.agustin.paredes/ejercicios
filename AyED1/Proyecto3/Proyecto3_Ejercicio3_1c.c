#include <assert.h>
#include <stdio.h>

int main (void) {
	int x, y;

	printf ("Introduzca un numero: ");
	scanf ("%d", &x);

	printf ("Introduzca otro numero: ");
	scanf ("%d", &y);

	//b). Puede comentarse la siguiente linea
	assert (x == 2 && y == 5);

	y += y; //y = y + y
	x += y; //x = x + y

	printf ("x: %d, y: %d\n", x, y);

	return 0;
}

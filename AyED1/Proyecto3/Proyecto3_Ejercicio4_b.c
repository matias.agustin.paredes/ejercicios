#include <stdio.h>

int main (void) {
	int x, y, z, m;

	printf ("Inserte el numero x: "); 
	scanf ("%d", &x);

	printf ("Inserte el numero y: "); 
	scanf ("%d", &y);

	printf ("Inserte el numero z: "); 
	scanf ("%d", &z);

	printf ("Inserte el numero m: "); 
	scanf ("%d", &m);

	if (x < y) {
		m = x;
	} else { 
		m = y;
	}

	if (m >= z) {
		m = z;
	}

	printf ("x: %d, y: %d, z: %d, m: %d\n", x, y, z, m);

	return 0;
}

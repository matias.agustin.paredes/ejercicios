#include <assert.h>
#include <stdio.h>

int main (void) {
	int x;

	printf ("Ingrese un numero: ");
	scanf ("%d", &x);

	//b). Puedes comentar la siguiente linea si quieres
	assert (x == 1);

	x = 5;

	printf ("x: %d\n", x);

	return 0;
}

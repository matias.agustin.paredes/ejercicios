#include <stdio.h>

int main (void) {
	int x, y;

	printf ("Inserte un numero: ");
	scanf ("%d", &x);

	printf ("Inserte otro numero: ");
	scanf ("%d", &y);

	if (x >= y) {
		x = 0;
	} else if (x <= y) {
		x = 2;
	}

	printf ("x: %d, y: %d\n", x, y);

	return 0;
}

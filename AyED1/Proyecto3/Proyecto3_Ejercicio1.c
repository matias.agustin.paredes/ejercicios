#include <stdbool.h>
#include <stdio.h>

//Funcion que imprime True o False segun corresponda
void imprime_bool (bool b) {
	if (b) {
		printf ("True\n");
	} else {
		printf ("False\n");
	}
}

int main (void) {
	int x, y, z;
	
	printf ("Introduzca el valor de x: ");
	scanf ("%d", &x);
	
	printf ("Introduzca el valor de y: ");
	scanf ("%d", &y);

	printf ("Introduzca el valor de z: ");
	scanf ("%d", &z);

	printf ("x + y + 1: %d\n", x + y + 1);
	printf ("z * z + y * 45 - 15 * x: %d\n", z * z + y * 45 - 15 * x);
	printf ("y - 2 == (x * 3 + 1) %% 5: ");
	
	imprime_bool (y - 2 == (x * 3 + 1) % 5);
	
	printf ("y / 2 * x: %d\n", y / 2 * x);
	printf ("y < x * z: ");
	
	imprime_bool (y < x * z);

	return 0;
}

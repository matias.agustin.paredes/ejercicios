#include <stdbool.h>
#include <stdio.h>

//Imprime un booleano
void print_bool (bool b) {
	if (b) {
		printf ("true\n");
	} else {
		printf ("false\n");
	}
}
	
int main (void) {
	int x, i, b; //b representa un booleano
	bool res;

	printf ("Ingrese un valor para x: ");
	scanf ("%d", &x);

	printf ("Ingrese un valor para i: ");
	scanf ("%d", &i);

	printf ("Ingrese un par para true, impar para false: ");
	scanf ("%d", &b);
	res = b % 2 == 0;

	i = 2;
	res = true;
	while (i < x && res) {
		res = res && x % i != 0;
		i += 1; //i = i + 1
		printf ("x: %d, i: %d, res: ", x, i);
		print_bool (res);
	}

	return 0;
}

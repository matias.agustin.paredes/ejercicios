--Ejercicio 1
--a)
esCero:: Int -> Bool
esCero n = n == 0

--b)
esPositivo:: Int -> Bool
esPositivo n = n > 0

--c)
esVocal:: Char -> Bool
esVocal x = x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u'

--Ejercicio 2
--a)
paraTodo:: [Bool] -> Bool
paraTodo [] = True
paraTodo (x:xs) = x && paraTodo xs

--b)
sumatoria:: [Int] -> Int
sumatoria [] = 0
sumatoria (x:xs) = x + sumatoria xs

--c)
productoria:: [Int] -> Int
productoria [] = 1
productoria (x:xs) = x * productoria xs

--d)
factorial:: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n - 1)

--e)
promedio:: [Int] -> Int
promedio xs = div (sumatoria xs) (length xs)

--Ejercicio 3
pertenece:: Int -> [Int] -> Bool
pertenece _ [] = False
pertenece n (x:xs) = n == x || pertenece n xs

--Ejercicio 4
--a)
paratodo':: [a] -> (a -> Bool) -> Bool
paratodo' [] _ = True
paratodo' (x:xs) f = f (x) && paratodo' xs f

--b)
existe':: [a] -> (a -> Bool) -> Bool
existe' [] _ = False
existe' (x:xs) f = f (x) || existe' xs f

--c)
sumatoria':: [a] -> (a -> Int) -> Int
sumatoria' [] _ = 0
sumatoria' (x:xs) f = f (x) + sumatoria' xs f

--d)
productoria':: [a] -> (a -> Int) -> Int
productoria' [] _ = 1
productoria' (x:xs) f = f (x) * productoria' xs f

--Ejercicio 5
esTrue:: Bool -> Bool
esTrue x = x

paraTodo'':: [Bool] -> Bool
paraTodo'' xs = paratodo' xs esTrue 

--Ejercicio 6
--a)
esPar:: Int -> Bool
esPar n = mod n 2 == 0

todosPares:: [Int] -> Bool
todosPares xs = paratodo' xs esPar

--b)
esMultiploDe:: Int -> Int -> Bool
esMultiploDe n m = mod m n == 0

hayMultiplo:: Int -> [Int] -> Bool
hayMultiplo n xs = existe' xs (esMultiploDe n)

--c)
cuadrado:: Int -> Int
cuadrado n = n * n

sumaCuadrados:: Int -> Int
sumaCuadrados n = sumatoria' [1..n] cuadrado

--d)
mismoNumero:: Int -> Int
mismoNumero n = n

factorial':: Int -> Int
factorial' n = productoria' [1..n] mismoNumero 

--e)
multiplicaPares:: [Int] -> Int
multiplicaPares xs = productoria' (filter esPar xs) mismoNumero

--Ejercicio 8
--a)
duplicaLista:: [Int] -> [Int]
duplicaLista [] = []
duplicaLista (x:xs) = x * 2 : duplicaLista xs

--b)
duplica:: Int -> Int
duplica n = n * 2

duplicaLista':: [Int] -> [Int]
duplicaLista' xs = map duplica xs

--Ejercicio 9
--a)
filtraPares:: [Int] -> [Int]
filtraPares [] = []
filtraPares (x:xs) | mod x 2 == 0 = x : filtraPares xs
                   | otherwise = filtraPares xs

--b)
filtraPares':: [Int] -> [Int]
filtraPares' xs = filter esPar xs

--c)
multiplicaPares':: [Int] -> Int
multiplicaPares' xs = productoria' (filtraPares' xs) mismoNumero

--Ejercicio 10
--a)
primIgualesA:: Eq a => a -> [a] -> [a]
primIgualesA _ [] = []
primIgualesA x (y:ys) | x == y = y : primIgualesA x ys
                      | otherwise = []

--b)
sonIguales:: Eq a => a -> a -> Bool
sonIguales a b = a == b

primIgualesA':: Eq a => a -> [a] -> [a]
primIgualesA' x ys = takeWhile (sonIguales x) ys

--Ejercicio 11
--a)
primIguales:: Eq a => [a] -> [a]
primIguales [] = []
primIguales [x] = [x]
primIguales (x:(y:ys)) | x == y = x : primIguales (y : ys)
                       | otherwise = [x]

--b)
primIguales':: Eq a => [a] -> [a]
primIguales' [] = []
primIguales' (x:xs) = primIgualesA' x (x:xs)

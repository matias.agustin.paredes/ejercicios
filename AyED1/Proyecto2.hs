--Definicion de Tipo de Datos Ej 1. a) y Ej 2. a)
data Carrera = Matematica | Fisica | Computacion | Astronomia deriving (Eq, Ord, Show)

--Definicion de Tipo de Datos Ej 3. a) y Ej 5.
type Ingreso = Int
data Cargo = Titular | Asociado | Adjunto | Asistente | Auxiliar deriving (Eq, Show)
data Area = Administrativa | Ensenanza | Economica | Postgrado deriving (Eq, Show)
data Persona = Decane
           | Docente Cargo
           | NoDocente Area
           | Estudiante Carrera Ingreso
           deriving (Eq, Show)

--Definicion de Tipo de Datos Ej 5.
data Cola = VaciaC | Encolada Persona Cola deriving Show

--Definicion de Tipo de Datos Ej 6.
data ListaAsoc a b = Vacia | Nodo a b (ListaAsoc a b) deriving Show
--Para este ejemplo usaremos una guia telefonica, como el Ejercicio 6. a)
type GuiaTelefonica = ListaAsoc String Int

--Definicion de Tipo de Datos Ej 7.
data Arbol a = Hoja | Rama (Arbol a) a (Arbol a) deriving Show
--Arboles de Strings forma Prefijos
type Prefijos = Arbol String

--Ejercicio 1
--b)
titulo:: Carrera -> String
titulo Matematica = "Licenciatura en Matematica"
titulo Fisica = "Licenciatura en Fisica"
titulo Computacion = "Licenciatura en Ciencias de la Computacion"
titulo Astronomia = "Licenciatura en Astronomia"

--Ejercicio 3
--c)
cuantosDoc:: [Persona] -> Cargo -> Int
cuantosDoc [] _ = 0
cuantosDoc (x:xs) c | x == Docente c = 1 + cuantosDoc xs c
                    | otherwise = cuantosDoc xs c

--Lista de ejemplo para ejecutar cuantosDoc
gente:: [Persona]
gente = [Decane, NoDocente Economica, NoDocente Economica, Docente Adjunto, Estudiante Astronomia 2021, NoDocente Administrativa, NoDocente Administrativa, Estudiante Computacion 2021, Docente Titular, Docente Titular, Docente Titular]

--d)
esDoc:: Cargo -> Persona -> Bool
esDoc c x = x == Docente c

cuantosDoc':: [Persona] -> Cargo -> Int
cuantosDoc' xs c = length (filter (esDoc c) xs)

--Ejercicio 4
--a)
primerElemento:: [a] -> Maybe a
primerElemento [] = Nothing
primerElemento (x:_) = Just (x)

--Ejercicio 5
--a)
--Cola de ejemplo
colaEjemplo:: Cola
colaEjemplo = Encolada Decane (Encolada (Docente Asistente) (Encolada (NoDocente Administrativa) (Encolada (Estudiante Astronomia 2021) (Encolada (Docente Titular) (Encolada (Docente Titular) VaciaC))))) 

--1)
atender:: Cola -> Maybe Cola
atender (Encolada _ c) = Just c
atender VaciaC = Nothing

--2)
encolar:: Persona -> Cola -> Cola
encolar p VaciaC = Encolada p VaciaC
encolar p (Encolada p2 c) = Encolada p2 (encolar p c)

--3)
busca:: Cola -> Cargo -> Maybe Persona
busca VaciaC _ = Nothing
busca (Encolada p cola) cargo | p == Docente cargo = Just p
                              | otherwise = busca cola cargo

--Ejercicio 6
--Guia de Ejemplo
guiaEjemplo:: GuiaTelefonica
guiaEjemplo = Nodo "Julian Paredes" 3515915868 (Nodo "Pedro Gigena" 3515805868 (Nodo "Matias Castro" 3515906868 Vacia))

guiaEjemplo2:: GuiaTelefonica
guiaEjemplo2 = Nodo "Pedro Paredes" 3515905878 (Nodo "Julian Castro" 3515905768 (Nodo "Matias Gigena" 3515905867 Vacia))

--1)
laLong:: ListaAsoc a b -> Int
laLong Vacia = 0
laLong (Nodo _ _ l) = 1 + laLong l

--2)
laConcat:: ListaAsoc a b -> ListaAsoc a b -> ListaAsoc a b
laConcat Vacia l = l
laConcat (Nodo a b l) l2 = Nodo a b (laConcat l l2)

--3)
laPares:: ListaAsoc a b -> [(a, b)]
laPares Vacia = []
laPares (Nodo a b l) = (a, b) : laPares l

--4)
laBusca:: Eq a => ListaAsoc a b -> a -> Maybe b
laBusca Vacia _ = Nothing
laBusca (Nodo a b l) a1 | a == a1 = Just b
                        | otherwise = laBusca l a1

laBorrar:: Eq a => a -> ListaAsoc a b -> ListaAsoc a b
laBorrar _ Vacia = Vacia
laBorrar a (Nodo a1 b l) | a == a1 = laBorrar a l
                         | otherwise = Nodo a1 b (laBorrar a l)

--Ejercicio 7
--Arbol de Ejemplo
can, cana, canario, canas, cant, cantar, canto:: Prefijos
can     = Rama cana "can" cant
cana    = Rama canario "a" canas
canario = Rama Hoja "rio" Hoja
canas   = Rama Hoja "s" Hoja
cant    = Rama cantar "t" canto
cantar  = Rama Hoja "ar" Hoja
canto   = Rama Hoja "o" Hoja

uno, dos, cuatro, ocho, diseis, treidos, secuatro:: Arbol Int
uno      = Rama dos 1 cuatro
dos      = Rama ocho 2 Hoja
cuatro   = Rama diseis 4 treidos
ocho     = Rama Hoja 8 Hoja
diseis   = Rama Hoja 16 secuatro
treidos  = Rama Hoja 32 Hoja
secuatro = Rama Hoja 64 Hoja

--a)
aLong:: Arbol a -> Int
aLong Hoja = 0
aLong (Rama a _ b) = 1 + aLong a + aLong b

--b)
aHojas:: Arbol a -> Int
aHojas Hoja = 1
aHojas (Rama a _ b) = aHojas a + aHojas b

--c)
aInc:: Num a => Arbol a -> Arbol a
aInc Hoja = Hoja
aInc (Rama a x b) = Rama (aInc a) (x + 1) (aInc b)

--d)
aMap:: (a -> b) -> Arbol a -> Arbol b
aMap _ Hoja = Hoja
aMap f (Rama a x b) = Rama (aMap f a) (f x) (aMap f b)

sumaUno:: Num a => a -> a
sumaUno x = x + 1

aInc':: Num a => Arbol a -> Arbol a
aInc' a = aMap sumaUno a

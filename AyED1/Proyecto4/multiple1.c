#include <assert.h>
#include "../Proyecto3/entradas.c"

int main (void) {
	int x, y, aux1, aux2; //Las ultimas dos son variable auxiliares para almacenar estados incilaes

	x = pedirEntero ('x');
	y = pedirEntero ('y');

	aux1 = x;
	aux2 = y;

	y += x; //y = y + x
	x += 1; //x = x + 1

	assert (aux1 + 1 == x && aux2 + aux1 == y);

	imprimeEntero (x, 'x');
	imprimeEntero (y, 'y');

	return 0;
}

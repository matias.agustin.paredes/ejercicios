#include <assert.h>
#include "../Proyecto3/entradas.c"

int main (void) {
	int x, y, res;

	x = pedirEntero ('x');
	y = pedirEntero ('y');

	if (x > y) {
		res = y;
	} else {
		res = x;
	}

	assert (res <= x && res <= y);

	imprimeEntero (res, 'r');

	return 0;
}

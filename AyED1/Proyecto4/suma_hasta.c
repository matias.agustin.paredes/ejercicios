#include <assert.h>
#include "../Proyecto3/entradas.c"

int suma_hasta (int n) {
	int i = 1, res = 0;

	while (i <= n) {
		res += i; //res = res + i
		i += 1; //i = i + 1
	}

	return res;
}

int main (void) {
	int n, res;

	n = pedirEntero ('n');

	assert (n > 0);

	res = suma_hasta (n);

	imprimeEntero (res, 'r');

	return 0;
}

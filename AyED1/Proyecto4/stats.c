#include <assert.h>
#include <limits.h>
#include <stdio.h>

struct datos_t {
	float maximo;
	float minimo;
	float promedio;
};

struct datos_t stats (float a[], int tam) {
	struct datos_t res;
	float sumatoria = 0.0;
	int i = 0;

	res.maximo = INT_MIN; //Seria algo asi como -infinito y +infinito respectivamente
	res.minimo = INT_MAX;

	while (i < tam) {
		if (a[i] >= res.maximo) {
			res.maximo = a[i];
		}
		if (a[i] <= res.minimo) {
			res.minimo = a[i];
		}
		
		sumatoria += a[i];

		i++;
	}

	res.promedio = sumatoria / tam;

	return res;
}

//Funcion que pide los elementos del arreglo. Similar al que se tiene en arreglos.c, pero modificado para aceptar floats
void pedirArreglo (float a[], int tam) {
	assert (tam >= 0);

	int i = 0;

	while (i < tam) {
		printf ("Ingrese el valor para la posicion %d: ", i);
		scanf ("%f", &a[i]);
		i++;
	}
}

int main (void) {
	struct datos_t res;
	int tam;

	printf ("Escoja el tamano del arreglo: ");
	scanf ("%d", &tam);

	float a[tam];

	pedirArreglo (a, tam);

	res = stats (a, tam);

	printf ("Maximo: %f\n", res.maximo);
	printf ("Minimo: %f\n", res.minimo);
	printf ("Promedio: %f\n", res.promedio);

	return 0;
}

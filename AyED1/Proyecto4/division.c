#include <assert.h>
#include "../Proyecto3/entradas.c"

struct div_t {
	int cociente;
	int resto;
};

struct div_t division (int x, int y) {
	struct div_t res;

	res.cociente = 0;

	while (x >= y) {
		x -= y; //x = x - y
		res.cociente += 1; //res.cociente = res.cociente + 1
	}

	res.resto = x;

	return res;
}

int main (void) {
	int x, y;
	struct div_t res;

	x = pedirEntero ('x');
	y = pedirEntero ('y');

	assert (x >= 0 && y > 0 && x >= y);

	res = division (x, y);

	imprimeEntero (res.cociente, 'c');
	imprimeEntero (res.resto, 'r');

	return 0;
}

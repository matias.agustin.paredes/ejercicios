#include "../Proyecto3/entradas.c"
#include "arreglos.c"

struct comp_t {
	int menores;
	int iguales;
	int mayores;
};

struct comp_t cuantos (int a[], int tam, int elem) {
	struct comp_t res;
	int i = 0;

	//No hacer esto arroja valores basura
	res.mayores = 0;
	res.iguales = 0;
	res.menores = 0;


	while (i < tam) {
		if (a[i] < elem) {
			res.menores++;
		} else if (a[i] == elem) {
			res.iguales++;
		} else { //else if (a[i] > elem)
			res.mayores++;
		}

		i++;
	}

	return res;
}

int main (void) {
	struct comp_t res;
	int n, tam;

	printf ("Elija el tamano del arreglo: ");
	scanf ("%d", &tam);

	int a[tam];

	pedirArreglo (a, tam);

	n = pedirEntero ('n');

	res = cuantos (a, tam, n);

	printf ("Menores que n: %d\n", res.menores);
	printf ("Iguales que n: %d\n", res.iguales);
	printf ("Mayores que n: %d\n", res.mayores);

	return 0;
}

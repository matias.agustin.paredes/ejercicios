#include <assert.h>
#include <stdio.h>

void pedirArreglo (int a[], int n_max) {
	assert (n_max >= 0);

	int i = 0;

	while (i < n_max) {
		printf ("Ingrese el valor para el numero %d: ", i + 1);
		scanf ("%d", &a[i]);
		i += 1; //i = i + 1
	}
}

void imprimeArreglo (int a[], int n_max) {
	assert (n_max >= 0);

	int i = 0;

	printf ("[");

	while (i < n_max) {
		printf ("%d", a[i]);

		if (i < n_max - 1) { //No es el ultimo elemento
			printf (", ");
		}

		i += 1; //i = i + 1
	}

	printf ("]\n");
}

//Se llama my_main2 para evitar colision de nombres. Para probar este ejercicio debe cambiarse el nombre a main
int my_main2 (void) {
	int length;

	printf ("Inserte longitud del arreglo: ");
	scanf ("%d", &length);

	assert (length >= 0);
	int a[length];

	pedirArreglo (a, length);
	imprimeArreglo (a, length);

	return 0;
}

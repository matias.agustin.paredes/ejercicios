#include "../Proyecto3/entradas.c"
#include "arreglos.c"

int sumatoria (int a[], int tam) {
	int res = 0, i = 0;

	while (i < tam) {
		res += a[i];
		i += 1;
	}

	return res;
}

int main (void) {
	int res;
	int a[5];

	pedirArreglo (a, 5);

	res = sumatoria (a, 5);

	imprimeEntero (res, 'r');

	return 0;
}

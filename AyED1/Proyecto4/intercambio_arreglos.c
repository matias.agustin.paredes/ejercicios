#include <assert.h>
#include "../Proyecto3/entradas.c"
#include "arreglos.c"

void intercambiar (int a[], int tam, int i, int j) {
	assert (i >= 0 && i < tam && j >= 0 && j < tam);

	int aux;

	aux = a[i];
	a[i] = a[j];
	a[j] = aux;
}

int main (void) {
	int i, j, tam;

	printf ("Ingrese el tamano del arreglo: ");
	scanf ("%d", &tam);

	int a[tam];

	pedirArreglo (a, tam);

	i = pedirEntero ('i');
	j = pedirEntero ('j');

	intercambiar (a, tam, i, j);

	imprimeArreglo (a, tam);

	return 0;
}

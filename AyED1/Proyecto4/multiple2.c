#include <assert.h>
#include "../Proyecto3/entradas.c"

int main (void) {
	int x, y, z, aux1, aux2, aux3; //Las ultimas dos son variable auxiliares para almacenar estados incilaes

	x = pedirEntero ('x');
	y = pedirEntero ('y');
	z = pedirEntero ('z');

	aux1 = x;
	aux2 = y;
	aux3 = z;

	y += x + z; // y = y + x + z
	z = y - z;
	x = z - x;

	assert (aux2 == x && aux2 + aux1 == z && aux2 + aux1 + aux3 == y);

	imprimeEntero (x, 'x');
	imprimeEntero (y, 'y');
	imprimeEntero (z, 'z');

	return 0;
}

#include <assert.h>
#include "../Proyecto3/entradas.c"

int main (void) {
	int x, y, z, aux; //aux almacenara el valor incial de y

	x = pedirEntero ('x');
	y = pedirEntero ('y');

	aux = y;

	z = x;
	x = y;
	y = z;

	assert (y == z && x == aux);

	imprimeEntero (x, 'x');
	imprimeEntero (y, 'y');

	return 0;
}
	

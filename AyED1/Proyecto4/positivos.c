#include <stdbool.h>
#include <stdio.h>
#include "arreglos.c"

bool existe_positivo (int a[], int tam) {
	int i = 0;
	bool res = false;

	while (i < tam) {
		res = res || a[i] > 0;
		i++; //i = i + 1
	}

	return res;
}

bool todos_positivos (int a[], int tam) {
	int i = 0;
	bool res = true;

	while (i < tam) {
		res = res && a[i] > 0;
		i++;
	}

	return res;
}

//Funcion que imprime un booleano en pantalla
void imprime_bool (bool b) {
	printf (b ? "true\n" : "false\n"); //Lo que esta dentro
	//del printf es como un if-else, donde el primer termino
	//es la guarda, el segundo lo que retorna si la guarda 
	//resulta en true y el tercero si resulta en false
	//En otras palabras, seria equivalente a:
	//if (b) {
	//	printf ("true\n");
	//} else {
	//	printf ("false\n");
	//}
}

bool ejecutar_funcion (int a[], int tam) {
	bool res, breaker = true;
	int input;

	//Se hace un bucle infinito a proposito
	while (breaker) {
		printf ("Ingrese 1 para ejecutar todos_positivos, 0 para ejecutar existe_positivo: ");
		scanf ("%d", &input);
		
		if (input == 0 || input == 1) {
			breaker = !breaker;
			if (input == 0) {
				res = existe_positivo (a, tam);
			} else {
				res = todos_positivos (a, tam);
			}
		} else {
			printf ("Numero invalido\n");
		}
	}

	return res;
}	

int main (void) {
	int a[5];
	bool res;

	pedirArreglo (a, 5);

	res = ejecutar_funcion (a, 5);

	printf ("Resultado: ");
	imprime_bool (res);

	return 0;
}

#include <assert.h>
#include "../Proyecto3/entradas.c"

int main (void) {
	int x, res;

	x = pedirEntero ('x');

	if (x >= 0) {
		res = x;
	} else {
		res = -x;
	}

	assert (res >= 0);

	imprimeEntero (res, 'r');

	return 0;
}

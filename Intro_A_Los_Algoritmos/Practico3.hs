promedio:: Float -> Float -> Float
promedio a b = (a + b) / 2

signo:: Int -> Int
signo x | x > 0 = 1
        | x < 0 = -1
        | otherwise = 0

entre0y9:: Int -> Bool
entre0y9 x = x >= 0 && x <= 9

rangoPrecio:: Int -> String
rangoPrecio x | x >= 0 && x < 2000 = "Muy Barato"
              | x >= 2000 && x <= 5000 = "Hay que verlo bien"
              | x > 5000 = "Demasiado Caro"
              | otherwise = "Esto no puede ser!"

absoluto:: Int -> Int
absoluto x | x >= 0 = x
           | otherwise = -x

esMultiplo2:: Int -> Bool
esMultiplo2 n = n `mod` 2 == 0

esMultiploDe:: Int -> Int -> Bool
esMultiploDe a b = b `mod` a == 0

esBisiesto:: Int -> Bool
esBisiesto n = esMultiploDe 400 n || (esMultiploDe 4 n && not (esMultiploDe 100 n))

maximo3Numeros:: Int -> Int -> Int -> Int
maximo3Numeros a b c = max (max a b) c

minimo3Numeros:: Int -> Int -> Int -> Int
minimo3Numeros a b c = min (min a b) c

dispersion:: Int -> Int -> Int -> Int
dispersion a b c = (maximo3Numeros a b c) - (minimo3Numeros a b c)

celsiusToFahr:: Float -> Float
celsiusToFahr n = n * 1.8 + 32

fahrToCelsius:: Float -> Float
fahrToCelsius n = (n - 32) / 1.8

haceFrio:: Float -> Bool
haceFrio n = (fahrToCelsius n) < 8

segundo3:: (Int, Int, Int) -> Int
segundo3 (a, b, c) = b 

ordena:: (Int, Int) -> (Int, Int)
ordena (a, b) = (min a b, max a b)

rangoValido:: Int -> Int -> Bool
rangoValido a b = a <= b && a >= 0

rangoPrecioParametrizado:: Int -> (Int, Int) -> String
rangoPrecioParametrizado x (a, b) | x >= 0 && x < a && rangoValido a b = "Muy Barato"
                                  | x >= a && x <= b && rangoValido a b = "Hay que verlo bien"
                                  | x > b && rangoValido a b = "Demasiado Caro"
                                  | otherwise = "Esto no puede ser!"

mayor3:: (Int, Int, Int) -> (Bool, Bool, Bool)
mayor3 (a, b, c) = (a > 3, b > 3, c > 3)

todosIguales:: (Int, Int, Int) -> Bool
todosIguales (a, b, c) = a == b && b == c

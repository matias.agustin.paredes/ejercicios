suma:: [Int] -> Int
suma [] = 0
suma (x:xs) = x +  suma xs

prod:: [Int] -> Int
prod [] = 0
prod [x] = x
prod (x:xs) = x *  prod xs

card:: [Int] -> Int
card [] = 0
card (x:xs) = 1 +  card xs

todosMenores10:: [Int] -> Bool
todosMenores10 [] = True
todosMenores10 (x:xs) = x < 10 && todosMenores10 xs

hay0:: [Int] -> Bool
hay0 [] = False
hay0 (x:xs) = x == 0 || hay0 xs

sumar1:: [Int] -> [Int]
sumar1 [] = []
sumar1 (x:xs) = (x + 1) : sumar1 xs

duplica:: [Int] -> [Int]
duplica [] = []
duplica (x:xs) = (x * 2) : duplica xs

multiplica:: Int -> [Int] -> [Int]
multiplica n [] = []
multiplica n (x:xs) = (n * x) : multiplica n xs

soloPares:: [Int] -> [Int]
soloPares [] = []
soloPares (x:xs) | mod x 2 == 0 = x : soloPares xs
                 | otherwise = soloPares xs

mayoresQue10:: [Int] -> [Int]
mayoresQue10 [] = []
mayoresQue10 (x:xs) | x > 10 = x : mayoresQue10 xs
                    | otherwise = mayoresQue10 xs

mayoresQue:: Int -> [Int] -> [Int]
mayoresQue n [] = []
mayoresQue n (x:xs) | x > n = x : mayoresQue n xs
                    | otherwise = mayoresQue n xs

cardinal:: [Int] -> Int
cardinal [] = 0
cardinal (x:xs) = 1 +  cardinal xs

indice:: Int -> [a] -> a
indice n (x:xs) | n == 0 = x
                | otherwise = indice (n - 1) xs

tomar:: Int -> [a] -> [a]
tomar n [] = []
tomar 0 xs = []
tomar n (x:xs) = x : tomar (n - 1) xs

tirar:: Int -> [a] -> [a]
tirar n [] = []
tirar 0 xs = xs
tirar n (x:xs) = tirar (n - 1) xs

concat2:: [a] -> [a] -> [a]
concat2 [] ys = ys
concat2 (x:xs) ys = x : concat2 xs ys

cabeza:: [a] -> a
cabeza xs = indice 0 xs

cola:: [a] -> [a]
cola xs = tirar 1 xs

acumular:: Int -> Int
acumular n | n <= 0 = 0
           | otherwise = n + acumular (n - 1)

factorial:: Int -> Int
factorial n | n < 0 = 0
            | n == 0 = 1
            | otherwise = n * factorial (n - 1)

sumaCuadrados:: Int -> Int
sumaCuadrados n | n <= 0 = 0
                | otherwise = n * n + sumaCuadrados (n - 1)

repetir:: Int -> Int -> Int
repetir n m | m <= 0 = 0
            | otherwise = n + repetir n (m - 1)

fibonacci:: Int -> Int
fibonacci n | n < 0 = 0
            | n == 1 || n == 0 = 1
            | otherwise = fibonacci (n - 1) + fibonacci (n - 2)

potencia:: Int -> Int -> Int
potencia b p | p < 0 = 0
             | p == 0 = 1
             | otherwise = b * potencia b (p - 1)

gente:: [String]
gente = ["Juan", "Maria"]

cartas1:: [String]
cartas1 = ["1deCopa", "3deOro", "7deEspada", "2deBasto"]

cartas2:: [String]
cartas2 = ["1deCopa"]

cartas3:: [String]
cartas3 = ["1deCopa", "3deOro"]

repartir:: [String] -> [String] -> [(String, String)]
repartir [] ys = []
repartir xs [] = []
repartir (x:xs) (y:ys) = (x, y) : repartir xs ys

alumnos:: [(String, String, Int)]
alumnos = [("Juan", "Dominguez", 22), ("Maria", "Gutierrez", 19), ("Damian", "Rojas", 18)]

apellidos:: [(String, String, Int)] -> [String]
apellidos [] = []
apellidos ((a, b, c):xs) = b : apellidos xs

maximo:: [Int] -> Int
maximo [x] = x
maximo (x:xs) = max x (maximo xs)

sumaPares:: [(Int, Int)] -> Int
sumaPares [] = 0
sumaPares ((a, b):xs) = a + b + sumaPares xs

todos0y1:: [Int] -> Bool
todos0y1 [] = True
todos0y1 (x:xs) = (x == 1 || x == 0) && todos0y1 xs

quitar0s:: [Int] -> [Int]
quitar0s [] = []
quitar0s (x:xs) | x /= 0 = x : quitar0s xs
                | otherwise = quitar0s xs

ultimo:: [a] -> a
ultimo [x] = x
ultimo (x:xs) = ultimo xs

repetir2:: Int -> Int -> [Int]
repetir2 0 k = []
repetir2 n k = k : repetir2 (n - 1) k

concat3:: [[a]] -> [a]
concat3 [] = []
concat3 (x:xs) = x ++ concat3 xs

rev:: [a] -> [a]
rev [] = []
rev (x:xs) = rev xs ++ [x]

palabra:: [Char]
palabra = ['f', 'a', 'm', 'a', 'f']

noEsta:: Char -> [Char] -> Bool
noEsta a [] = True
noEsta a (x:xs) = a /= x && noEsta a xs

sumaPond:: [(Int, Int)] -> Int
sumaPond [] = 0
sumaPond ((a, b):xs) = a * b + sumaPond xs

listaPalabras1:: [String]
listaPalabras1 = ["ocelote", "murcielago", "proboscidio"]

listaPalabras2:: [String]
listaPalabras2 = ["ocelote", "troll", "proboscidio"]

sinA:: [String] -> [String]
sinA [] = []
sinA (x:xs) | noEsta 'a' x = x : sinA xs
            | otherwise = sinA xs

listasIguales:: Eq a => [a] -> [a] -> Bool
listasIguales [] [] = True
listasIguales [] ys = False
listasIguales xs [] = False
listasIguales (x:xs) (y:ys) = x == y && listasIguales xs ys

maximo3Numeros:: Int -> Int -> Int -> Int
maximo3Numeros a b c = max (max a b) c

notas:: [(String, Int, Int, Int)]
notas = [("Matias", 7, 7, 8), ("Juan", 10, 6, 9), ("Lucas", 2, 10, 10)]

mejorNota:: [(String, Int, Int, Int)] -> [(String, Int)]
mejorNota [] = []
mejorNota ((a, b, c, d):xs) = (a, maximo3Numeros b c d) : mejorNota xs

incPrim:: [(Int, Int)] -> [(Int, Int)]
incPrim [] = []
incPrim ((a, b):xs) = (a + 1, b) : incPrim xs

expandir:: String -> String
expandir "" = ""
expandir (x:xs) | length (x:xs) > 1 = x : (' ' : expandir xs)
                | otherwise = x : xs

peliculas:: [(String, Int, Int, String)]
peliculas = [("¿Quieres ser John Malkovich?", 1999, 112,"Spike Jonze"),
             ("¿Y donde esta el piloto?", 1980, 88,"Jim Abrahams, David Zucker"),
             ("A Clockwork Orange", 1971, 136,"Stanley Kubrick"),
             ("America X", 1998, 119,"Tony Kaye"),
             ("Amor eterno", 2004, 133,"Jean-Pierre Jeunet"),
             ("Analizame", 1999, 103,"Harold Ramis"),
             ("Asesinos por naturaleza", 1994, 118,"Oliver Stone"),
             ("Borat: El segundo mejor reportero del glorioso pais Kazajistan viaja a America", 2006, 84,"Larry Charles"),
             ("Brüno", 2009, 81,"Larry Charles"),
             ("Buenos muchachos", 1990, 146,"Martin Scorsese"),
             ("Ciudad de Dios", 2002, 130,"Fernando Meirelles, Katia Lund"),
             ("Cloud Atlas: La red invisible", 2012, 172,"Tom Tykwer, Andy Wachowski"),
             ("Delicatessen", 1991, 99,"Marc Caro, Jean-Pierre Jeunet"),
             ("Django sin cadenas", 2012, 165,"Quentin Tarantino"),
             ("El abogado del diablo", 1997, 144,"Taylor Hackford"),
             ("El ciudadano", 1941, 119,"Orson Welles"),
             ("El club de la pelea", 1999, 139,"David Fincher"),
             ("El cocodrilo", 1999, 82,"Steve Miner"),
             ("El embajador del miedo", 2004, 129,"Jonathan Demme"),
             ("El habitante incierto", 2004, 90,"Guillem Morales"),
             ("El ilusionista", 2006, 110,"Neil Burger"),
             ("El maquinista", 2004, 101,"Brad Anderson"),
             ("El mundo esta loco loco", 2001, 112,"Jerry Zucker"),
             ("El padrino", 1972, 175,"Francis Ford Coppola"),
             ("El pianista", 2002, 150,"Roman Polanski"),
             ("El plan perfecto", 2006, 129,"Spike Lee"),
             ("El resplandor", 1980, 100,"Stanley Kubric"),
             ("El senor de los anillos: La comunidad del anillo", 2001, 178,"Peter Jackson"),
             ("Estamos todos locos", 1983, 107,"Terry Jones, Terry Gilliam"),
             ("Eterno resplandor de una mente sin recuerdos", 2004, 108,"Michel Gondry"),
             ("Ganster americano", 2007, 157,"Ridley Scott"),
             ("Gran Torino", 2008, 116,"Clint Eastwood"),
             ("Guerra de los mundos", 2005, 116,"Steven Spielberg"),
             ("Hechizo del tiempo", 1993, 101,"Harold Ramis"),
             ("Historias cruzadas", 2011, 146,"Tate Taylor"),
             ("Juegos sexuales", 1999, 97,"Roger Kumble"),
             ("Kill Bill, la venganza: Volumen I", 2003, 111,"Quentin Tarantino"),
             ("La caida", 2004, 156,"Oliver Hirschbiegel"),
             ("La edad de la inocencia", 1993, 139,"Martin Scorsese"),
             ("La niebla", 2007, 126,"Frank Darabont"),
             ("La noche del demonio", 2010, 103,"James Wan"),
             ("La ola", 2008, 107,"Dennis Gansel"),
             ("La vida de Brian", 1979, 94,"Terry Jones"),
             ("La vida de los otros", 2006, 137,"Florian Henckel von Donnersmarck"),
             ("Los caballeros de la mesa cuadrada", 1975, 91,"Terry Gilliam, Terry Jones"),
             ("Los otros", 2001, 101,"Alejandro Amenabar"),
             ("Los sospechosos de siempre", 1995, 106,"Bryan Singer"),
             ("Magdalene Sisters: En el nombre de Dios", 2002, 119,"Peter Mullan"),
             ("Magnolia", 1999, 188,"Paul Thomas Anderson"),
             ("Martha Marcy May Marlene", 2011, 102,"Sean Durkin"),
             ("Matrix", 1999, 136,"The Wachowski Brothers, The Wachowski Brothers"),
             ("Mississippi en llamas", 1988, 128,"Alan Parker"),
             ("Numero 23", 2007, 101,"Joel Schumacher"),
             ("Pandillas de Nueva York", 2002, 167,"Martin Scorsese"),
             ("Perros de la calle", 1992, 99,"Quentin Tarantino"),
             ("Petroleo sangriento", 2007, 158,"Paul Thomas Anderson"),
             ("Pi", 1998, 84,"Darren Aronofsky"),
             ("Promesas del este", 2007, 100,"David Cronenberg"),
             ("Psicopata americano", 2000, 102,"Mary Harron"),
             ("Requiem para un sueno", 2000, 102,"Darren Aronofsky"),
             ("Suenos de libertad", 1994, 142,"Frank Darabont"),
             ("Taxi Driver", 1976, 113,"Martin Scorsese"),
             ("The Butcher Boy", 1997, 110,"Neil Jordan"),
             ("The Weather Man", 2005, 102,"Gore Verbinski")]

verTodas:: [(String, Int, Int, String)] -> Int
verTodas [] = 0
verTodas ((a, b, c, d):xs) = c + verTodas xs

estrenos:: [(String, Int, Int, String)] -> [String]
estrenos [] = []
estrenos ((a, b, c, d):xs) | b == 2020 = a : estrenos xs
                           | otherwise = estrenos xs

filmografia:: [(String, Int, Int, String)] -> String -> [String]
filmografia [] director = []
filmografia ((a, b, c, d):xs) director | d == director = a : filmografia xs director
                                       | otherwise = filmografia xs director

duracion:: [(String, Int, Int, String)] -> String -> Int
duracion [] peli = 0
duracion ((a, b, c, d):xs) peli | a == peli = c
                                | otherwise = duracion xs peli

/* First, the standard lib includes, alphabetically ordered */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>


/* Maximum allowed length of the array */
#define MAX_SIZE 100000

unsigned int array_from_stdin(int array[],
           unsigned int max_size) {
    unsigned int size = 0;

    printf ("Ingrese taman�o del arreglo: ");
    fscanf (stdin, "%u", &size);

    assert (size < max_size);

    for (unsigned int i = 0; i < size; ++i) {
	printf ("Ingrese el numero de la posicion %d: ", i);
        fscanf (stdin, "%d", &array[i]);
    }

    return size; 
}

void array_dump(int a[], unsigned int length) {
    printf ("[");

    for (unsigned int i = 0; i < length; ++i) {
        printf ("%d", a[i]);

	if (i < length - 1) {
	    printf (", ");
	}
    }

    printf ("]\n");
}


int main(){
    /* create an array of MAX_SIZE elements */
    int array[MAX_SIZE];
    
    /* parse the file to fill the array and obtain the actual length */
    unsigned int length = array_from_stdin(array, MAX_SIZE);
    
    /*dumping the array*/
    array_dump(array, length);
    
    return (EXIT_SUCCESS);
}

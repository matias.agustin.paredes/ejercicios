#include <assert.h>
#include <stdio.h>
#include "array_helpers.h"

unsigned int array_from_file(int array[],
           unsigned int max_size,
           const char *filepath) {
    FILE *file = fopen (filepath, "r");
    unsigned int size = 0;

    fscanf (file, "%u\n", &size);

    assert (size < max_size);

    for (unsigned int i = 0; i < size; ++i) {
	if (i < size - 1) {
            fscanf (file, "%d ", &array[i]);
	} else {
	    fscanf (file, "%d", &array[i]);
	}
    }

    fclose (file);

    return size; 
}

void array_dump(int a[], unsigned int length) {
    printf ("[");

    for (unsigned int i = 0; i < length; ++i) {
        printf ("%d", a[i]);

	if (i < length - 1) {
	    printf (", ");
	}
    }

    printf ("]\n");
}

bool array_is_sorted(int a[], unsigned int max_size) {
    bool result = true;

    for (unsigned int i = 0; i < max_size - 1 && result; ++i) {
        result = result && a[i] <= a[i + 1];
    }

    return result;
}    

#ifndef ARRAY_HELPERS_H_
#define ARRAY_HELPERS_H_

unsigned int array_from_file(int array[], unsigned int max_size, const char *filepath);
void array_dump(int a[], unsigned int length);

#endif
